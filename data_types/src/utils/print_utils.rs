pub fn print_separator_with_text(text: &str, line_char: &str) {
    const LINE_LENGTH: usize = 80;
    let text_length = text.len();
    let left_padding = (LINE_LENGTH - text_length) / 2;
    let right_padding = LINE_LENGTH - text_length - left_padding;
    println!(
        "{} {} {}",
        line_char.repeat(left_padding),
        text,
        line_char.repeat(right_padding)
    );
}
