use crate::utils::print_separator_with_text;

pub fn declaring_variables() {
    // Declare an immutable variable x with value 5
    // The type of x is implicitly inferred from the value
    let x = 5;
    println!("The value of x is: {}", x);

    // Declare a mutable variable y with value 10
    // The type of y is explicitly inferred from the value
    let y: i32 = 10;
    println!("The value of y is: {}", y);

    // This declaration will shadow the previous declaration of x
    let x = 6i32;
    println!("The value of shadowing x is: {}", x);

    // Declares a mutable variable x with value 10
    let mut x = 10;
    println!("The value of mutable x is: {}", x);
    // Since x is mutable, we can change the value of x
    x += 5;
    println!("The value of mutable x after mutating is: {}", x);
}

pub fn data_types() {
    let line_char = "*";

    // Scalar Types
    print_separator_with_text("2.1 Scalar Types", line_char);
    {
        // Integer types
        // i8, i16, i32, i64, i128, isize
        print_separator_with_text("2.1.1 Signed integer Types", line_char);
        {
            let x: i32 = 5; // i32
            let y: i64 = 10; // i64

            println!("The value of x is: {}", x);
            println!("The value of y is: {}", y);
        }

        // Unsigned integer types
        // u8, u16, u32, u64, u128, usize
        print_separator_with_text("2.1.2 Unsigned Integer Types", line_char);
        {
            let x: u32 = 5; // u32
            let y: u64 = 10; // u64

            println!("The value of x is: {}", x);
            println!("The value of y is: {}", y);
        }

        // Floating point types
        // f32, f64
        print_separator_with_text("2.1.3 Float Types", line_char);
        {
            let x: f64 = 5.0; // f64
            let y: f32 = 10.0; // f32

            println!("The value of x is: {:.2}", x);
            println!("The value of y is: {:.2}", y);
        }

        // Boolean type
        // bool (true, false)
        print_separator_with_text("2.1.4 Boolean Types", line_char);
        {
            let x: bool = true;
            let y: bool = false;

            println!("The value of x is: {}", x);
            println!("The value of y is: {}", y);
        }

        // Character type
        // char (Unicode scalar values 4 bytes in size)
        print_separator_with_text("2.5 Character Types", line_char);
        {
            let x: char = 'x';
            let y: char = 'y';

            println!("The value of x is: {}", x);
            println!("The value of y is: {}", y);
        }
    }

    // Compund Types
    print_separator_with_text("2.2 Compound Types", line_char);
    {
        // Tuple type
        // (i32, f64, u8)
        print_separator_with_text("2.2.1 Tuple Types", line_char);
        {
            let tup: (i32, f64, u8) = (500, 6.4, 1);
            println!("The value of tup is: {:?}", tup);
            println!(
                "Individual values of tup are: {} {} {}",
                tup.0, tup.1, tup.2
            );

            // Tuple destructuring
            print_separator_with_text("2.2.1.1 Tuple Destructuring", line_char);
            {
                let (x, y, z) = tup;

                println!("The value of x is: {}", x);
                println!("The value of y is: {}", y);
                println!("The value of z is: {}", z);
            }
        }

        // Array type
        print_separator_with_text("2.2.2 Array Types", line_char);
        {
            let arr: [i32; 3] = [1, 2, 3];
            println!("The value of arr is: {:?}", arr);

            // Accessing array elements
            print_separator_with_text("2.2.2.1 Accessing Array Elements", line_char);
            {
                let x = arr[0];
                let y = arr[1];
                let z = arr[2];

                println!("The value of x is: {}", x);
                println!("The value of y is: {}", y);
                println!("The value of z is: {}", z);
            }

            // Array destructuring
            print_separator_with_text("2.2.2.2 Array Destructuring", line_char);
            {
                let [x, y, z] = arr;

                println!("The value of x is: {}", x);
                println!("The value of y is: {}", y);
                println!("The value of z is: {}", z);
            }

            // Array type with initial value
            print_separator_with_text("2.2.2.3 Array with Initial Value", line_char);
            {
                let arr: [i32; 10] = [1; 10];

                println!("The value of arr is: {:?}", arr);
            }

            // Array slices
            print_separator_with_text("2.2.2.4 Array Slices", line_char);
            {
                let arr: [i32; 10] = [1; 10];
                let slice: &[i32] = &arr[0..5];

                println!("The value of slice is: {:?}", slice);
            }
        }
    }

    print_separator_with_text("2.3 Custom Types", line_char);
    // Custom Types
    {
        // Struct type
        print_separator_with_text("2.3.1 Struct Types", line_char);
        {
            // Declaring a struct
            // Or a tuple struct struct Point(i32, i32);
            #[derive(Debug)]
            struct Point {
                x: i32,
                y: i32,
            }

            // Creating a struct instance
            let p = Point { x: 10, y: 20 };
            println!("The value of p is: {:?}", p);
            println!("The value of p.x is: {}", p.x);
            println!("The value of p.y is: {}", p.y);

            print_separator_with_text("2.3.1.1 Destructuring Structs", line_char);
            {
                let Point { x, y: p_y } = p;
                println!("The value of x is: {}", x);
                println!("The value of y is: {}", p_y);
            }

            print_separator_with_text("2.3.1.2 Struct Update Syntax", line_char);
            {
                let p1 = Point { x: 10, y: 20 };
                let p2 = Point { x: 30, ..p1 };
                println!("The value of p2 is: {:?}", p2);
            }
        }

        // Enum type
        print_separator_with_text("2.3.2 Enum Types", line_char);
        {
            // Declaring an enum
            enum WebEvent {
                PageLoad,                 // unit type
                KeyPress(char),           // tuple type
                Click { x: i64, y: i64 }, // c-like struct type
            }

            // Creating an enum instance
            let e1 = WebEvent::PageLoad;
            let e2 = WebEvent::KeyPress('x');
            let e3 = WebEvent::Click { x: 10, y: 20 };

            fn inspect(event: WebEvent) {
                match event {
                    WebEvent::PageLoad => println!("Page loaded"),
                    WebEvent::KeyPress(c) => println!("Key pressed: {}", c),
                    WebEvent::Click { x, y } => println!("Clicked at x: {}, y: {}", x, y),
                }
            }

            inspect(e1);
            inspect(e2);
            inspect(e3);
        }
    }
}

pub fn variable_life_cycle() {
    let s = "Hello from outside";
    {
        let s = "Hello from inside"; // s is shadowed here
        println!("{}", s);
    } // this scope is now over, and s is no longer valid
    println!("{}", s);
}
