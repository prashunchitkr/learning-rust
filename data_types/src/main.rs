mod utils;
mod variables;

use utils::print_separator_with_text;

fn main() {
    let line_char = "=";
    print_separator_with_text("1 Declaring Variables", line_char);
    variables::declaring_variables();
    print_separator_with_text("2 Data Types", line_char);
    variables::data_types();
    print_separator_with_text("3. Variable Life Cycle", line_char);
    variables::variable_life_cycle();
}
